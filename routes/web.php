<?php

declare(strict_types=1);

Route::fallback(function () {
    return response()->json([
        'message' => 'Page Not Found',
    ], 404);
});
