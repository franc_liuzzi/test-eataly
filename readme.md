# Test e-commerce backend (Sviluppo)

## Scenario

![Scenario](scenario.png)

## Richiesta

Il candidato deve implementare la componente B dello scenario precedente. Le API da implementare sono definite dal seguente schema: https://generator.swagger.io/?url=https://enet-playground.s3-eu-west-1.amazonaws.com/devtest/swagger.json

## Implementazione

Il progetto è stato realizzato utilizzando il framework Laravel. Ci sono due feature Test: GetOrderTest e PlaceOrderTest che testano il corretto funzionamento dei due endpoint definiti nello schema swagger.

- Per avviare il server: ```docker-compose up app```
- Per eseguire i test: ```docker-compose exec app composer tests```
