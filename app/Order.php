<?php

declare(strict_types=1);

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Order class.
 *
 * @property Customer        $customer
 * @property ShippingAddress $shippingAddress
 */
class Order extends Model
{
    protected $fillable = [
        'customer_id',
        'shipping_address_id',
        'total',
    ];

    public function customer(): BelongsTo
    {
        return $this->belongsTo('App\Customer');
    }

    public function shippingAddress(): BelongsTo
    {
        return $this->belongsTo('App\ShippingAddress');
    }

    public function orderItems(): HasMany
    {
        return $this->hasMany('App\OrderItem');
    }

    public function items(): BelongsToMany
    {
        return $this->belongsToMany('App\Item', 'App\OrderItem');
    }
}
