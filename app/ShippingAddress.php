<?php

declare(strict_types=1);

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShippingAddress extends Model
{
    protected $fillable = [
        'customer_id',
        'street',
        'city',
        'zipcode',
        'country',
    ];

    public function customer()
    {
        return $this->belongsTo('App\Customer');
    }
}
