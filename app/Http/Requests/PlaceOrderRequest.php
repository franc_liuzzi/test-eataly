<?php

declare(strict_types=1);

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PlaceOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'customer_email' => 'required|email',
            'customer_firstname' => 'required|string|max:255',
            'customer_lastname' => 'required|string|max:255',

            'shipping_address.street' => 'required|string|max:255',
            'shipping_address.city' => 'required|string|max:255',
            'shipping_address.zipcode' => 'required|string|max:10',
            'shipping_address.country' => 'required|string|max:255',

            'order_items.*.sku' => 'required|distinct|string|max:10',
            'order_items.*.qty' => 'required|numeric',

            'order_total' => 'numeric',
        ];
    }
}
