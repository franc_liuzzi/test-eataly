<?php

declare(strict_types=1);

namespace App\Http\Controllers\Order;

use App\Http\Controllers\Controller;
use App\Services\Order\GetOrder\GetOrderServiceInterface;

class GetOrder extends Controller
{
    public function __invoke(GetOrderServiceInterface $getOrderService)
    {
        $data = $getOrderService->buildResponse();

        return response($data);
    }
}
