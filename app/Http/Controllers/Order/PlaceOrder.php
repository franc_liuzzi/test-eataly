<?php

declare(strict_types=1);

namespace App\Http\Controllers\Order;

use App\Http\Controllers\Controller;
use App\Services\Order\PlaceOrder\PlaceOrderServiceInterface;

class PlaceOrder extends Controller
{
    public function __invoke(PlaceOrderServiceInterface $placeOrderService)
    {
        $order = $placeOrderService->placeOrder();

        return redirect()
            ->route('get_order', [
                'order' => $order->id,
            ]);
    }
}
