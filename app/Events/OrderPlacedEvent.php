<?php

declare(strict_types=1);

namespace App\Events;

use App\Order;
use Illuminate\Queue\SerializesModels;

/**
 * OrderPlacedEvent class.
 *
 * @property Order $order
 */
class OrderPlacedEvent
{
    use SerializesModels;

    public $order;

    /**
     * Create a new event instance.
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }
}
