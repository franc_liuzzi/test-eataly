<?php

declare(strict_types=1);

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * OrderItem class.
 *
 * @property Order $order
 * @property Item  $item
 */
class OrderItem extends Model
{
    protected $fillable = [
        'order_id',
        'item_id',
        'qty',
    ];

    public function order()
    {
        return $this->belongsTo('App\Order');
    }

    public function item()
    {
        return $this->belongsTo('App\Item');
    }
}
