<?php

declare(strict_types=1);

namespace App\Listeners;

use App\Events\OrderPlacedEvent;
use Illuminate\Support\Facades\Log;

class SendToMiddlewareListener
{
    /**
     * Create the event listener.
     */
    public function __construct()
    {
    }

    /**
     * Handle the event.
     *
     * @param OrderPlacedEvent $event
     */
    public function handle(OrderPlacedEvent $event)
    {
        Log::debug('send order to WMS integrator queue');
    }
}
