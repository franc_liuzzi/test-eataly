<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $fillable = [
        'email',
        'firstname',
        'lastname'
    ];

    public function shippingAddresses()
    {
        return $this->hasMany('App\ShippingAddress');
    }

    public function orders()
    {
        return $this->hasMany('App\Order');
    }
}
