<?php

declare(strict_types=1);

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $fillable = [
        'sku',
        'description',
        'price',
    ];

    public function orderItems()
    {
        return $this->hasMany('App\OrderItem');
    }

    public function orders()
    {
        return $this->hasManyThrough('App\Order', 'App\OrderItem');
    }
}
