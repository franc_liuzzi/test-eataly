<?php

declare(strict_types=1);

namespace App\Services\Order\PlaceOrder;

use App\Customer;
use App\Events\OrderPlacedEvent;
use App\Http\Requests\PlaceOrderRequest;
use App\Item;
use App\Order;
use App\OrderItem;
use App\ShippingAddress;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/**
 * PlaceOrderService class.
 *
 * @property Request $request
 */
class PlaceOrderService implements PlaceOrderServiceInterface
{
    private $request;

    public function __construct(PlaceOrderRequest $request)
    {
        $this->request = $request;
    }

    public function placeOrder(): Order
    {
        $customer_email = $this->request->input('customer_email');
        $customer_firstname = $this->request->input('customer_firstname');
        $customer_lastname = $this->request->input('customer_lastname');

        $shipping_address_street = $this->request->input('shipping_address.street');
        $shipping_address_city = $this->request->input('shipping_address.city');
        $shipping_address_zipcode = $this->request->input('shipping_address.zipcode');
        $shipping_address_country = $this->request->input('shipping_address.country');

        $order_items = $this->request->input('order_items');

        $order_total = $this->request->input('order_total');

        try {
            DB::beginTransaction();

            $order = new Order();

            $customer = Customer::firstOrCreate([
                'email' => $customer_email,
                'firstname' => $customer_firstname,
                'lastname' => $customer_lastname,
            ]);

            $shipping_address = ShippingAddress::firstOrCreate([
                'customer_id' => $customer->id,
                'street' => $shipping_address_street,
                'city' => $shipping_address_city,
                'zipcode' => $shipping_address_zipcode,
                'country' => $shipping_address_country,
            ]);

            $shipping_address->customer()->associate($customer)->save();

            $order->customer()->associate($customer);
            $order->shippingAddress()->associate($shipping_address);

            $order->total = $order_total;

            $order->save();

            $order->orderItems()->saveMany(collect($order_items)->map(function ($orderItem) {
                $item = Item::firstOrCreate(
                    ['sku' => $orderItem['sku']],
                    [
                        'description' => 'Testing Description',
                        'price' => 9.99,
                    ]
                );

                return new OrderItem([
                    'item_id' => $item->id,
                    'qty' => $orderItem['qty'],
                ]);
            }));

            DB::commit();

            event(new OrderPlacedEvent($order));

            return $order;
        } catch (Exception $exception) {
            DB::rollback();
            throw $exception;
        }
    }
}
