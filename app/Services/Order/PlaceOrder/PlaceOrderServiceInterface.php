<?php

declare(strict_types=1);

namespace App\Services\Order\PlaceOrder;

use App\Order;

interface PlaceOrderServiceInterface
{
    public function placeOrder(): Order;
}
