<?php

declare(strict_types=1);

namespace App\Services\Order\GetOrder;

use App\Order;
use Illuminate\Http\Request;

/**
 * GetOrderService class.
 *
 * @property Order $order
 */
class GetOrderService implements GetOrderServiceInterface
{
    public $order;

    public function __construct(Request $request)
    {
        $this->order = $request->route('order') instanceof Order ? $request->route('order') : Order::find($request->route('order'));
    }

    public function buildResponse(): array
    {
        return self::buildResponseFromOrder($this->order);
    }

    public static function buildResponseFromOrder($order): array
    {
        return [
            'order_id' => $order->id,

            'customer_email' => $order->customer->email,
            'customer_firstname' => $order->customer->firstname,
            'customer_lastname' => $order->customer->lastname,

            'shipping_address' => [
                'street' => $order->shippingAddress->street,
                'city' => $order->shippingAddress->city,
                'zipcode' => $order->shippingAddress->zipcode,
                'country' => $order->shippingAddress->country,
            ],

            'order_items' => $order->orderItems->map(function ($orderItem) {
                return [
                    'sku' => $orderItem->item->sku,
                    'qty' => $orderItem->qty,
                ];
            }),

            'order_total' => $order->total,
        ];
    }
}
