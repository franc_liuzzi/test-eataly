<?php

declare(strict_types=1);

namespace App\Services\Order\GetOrder;

interface GetOrderServiceInterface
{
    public function buildResponse(): array;
}
