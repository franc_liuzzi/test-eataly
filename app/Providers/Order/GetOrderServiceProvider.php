<?php

declare(strict_types=1);

namespace App\Providers\Order;

use Illuminate\Support\ServiceProvider;

class GetOrderServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register()
    {
    }

    /**
     * Bootstrap services.
     */
    public function boot()
    {
        $this->app->bind('App\Services\Order\GetOrder\GetOrderServiceInterface', 'App\Services\Order\GetOrder\GetOrderService');
    }
}
