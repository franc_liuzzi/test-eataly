<?php

declare(strict_types=1);

namespace App\Providers\Order;

use Illuminate\Support\ServiceProvider;

class PlaceOrderServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register()
    {
    }

    /**
     * Bootstrap services.
     */
    public function boot()
    {
        $this->app->bind('App\Services\Order\PlaceOrder\PlaceOrderServiceInterface', 'App\Services\Order\PlaceOrder\PlaceOrderService');
    }
}
