<?php

declare(strict_types=1);

namespace Tests\Feature;

use App\Order;
use Tests\TestCase;

class GetOrderTest extends TestCase
{
    /**
     * @dataProvider provider
     * @test
     */
    public function get_order_and_check_structure(
        $orderID
    ) {
        $response = $this->get("/api/v1/order/$orderID");

        $response->assertStatus(200);

        return $response->assertJsonStructure([
            'order_id',
            'customer_email',
            'customer_firstname',
            'customer_lastname',
            'shipping_address' => [
                'street',
                'city',
                'zipcode',
                'country',
            ],
            'order_items' => [
                '*' => [
                    'sku',
                    'qty',
                ],
            ],
            'order_total',
        ]);
    }

    /**
     * @dataProvider provider
     * @test
     */
    public function get_order_and_check_data(
        $orderID
    ) {
        $response = $this->get("/api/v1/order/$orderID");

        $response->assertStatus(200);

        $order = Order::find($orderID);

        $response->assertJson(\json_decode(\json_encode([
            'order_id' => $order->id,

            'customer_email' => $order->customer->email,
            'customer_firstname' => $order->customer->firstname,
            'customer_lastname' => $order->customer->lastname,

            'shipping_address' => [
                'street' => $order->shippingAddress->street,
                'city' => $order->shippingAddress->city,
                'zipcode' => $order->shippingAddress->zipcode,
                'country' => $order->shippingAddress->country,
            ],

            'order_items' => $order->orderItems
                ->map(function ($orderItem) {
                    return [
                        'sku' => $orderItem->item->sku,
                        'qty' => $orderItem->qty,
                    ];
                }),

            'order_total' => $order->total,
        ]), true));
    }

    public function provider()
    {
        $order = factory(\App\Order::class)->create();
        $anotherOrder = factory(\App\Order::class)->create();

        return [
            [
                $order->id,
            ],
            [
                $anotherOrder->id,
            ],
        ];
    }
}
