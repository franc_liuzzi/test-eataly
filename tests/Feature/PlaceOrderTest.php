<?php

declare(strict_types=1);

namespace Tests\Feature;

use Tests\TestCase;

class PlaceOrderTest extends TestCase
{
    /**
     * @dataProvider provider
     * @test
     */
    public function should_place_a_new_order(
        $data
    ) {
        $response = $this->json('post', '/api/v1/order', $data);

        $response->assertRedirect();

        $this->assertDatabaseHas('customers', [
            'email' => $data['customer_email'],
            'firstname' => $data['customer_firstname'],
            'lastname' => $data['customer_lastname'],
        ]);

        $this->assertDatabaseHas('shipping_addresses', [
            'street' => $data['shipping_address']['street'],
            'city' => $data['shipping_address']['city'],
            'zipcode' => $data['shipping_address']['zipcode'],
            'country' => $data['shipping_address']['country'],
        ]);
    }

    public function provider()
    {
        return [
            [
                [
                    'customer_email' => 'johnny@eataly.it',
                    'customer_firstname' => 'John',
                    'customer_lastname' => 'Doe',
                    'shipping_address' => [
                        'street' => 'Via da qui 21',
                        'city' => 'Milano',
                        'zipcode' => '20100',
                        'country' => 'IT',
                    ],
                    'order_items' => [
                    [
                        'sku' => '81848',
                        'qty' => 1,
                    ],
                    ],
                    'order_total' => 85.52,
                ],
            ],
            [
                [
                    'customer_email' => 'mario@eataly.it',
                    'customer_firstname' => 'Mario',
                    'customer_lastname' => 'Rossi',
                    'shipping_address' => [
                    'street' => 'Via da li 22',
                    'city' => 'Bari',
                    'zipcode' => '73100',
                    'country' => 'IT',
                    ],
                    'order_items' => [
                        [
                            'sku' => '4287844',
                            'qty' => 2,
                        ],
                        [
                            'sku' => '34988433',
                            'qty' => 1,
                        ],
                    ],
                    'order_total' => 45.98,
                ],
            ],
        ];
    }
}
