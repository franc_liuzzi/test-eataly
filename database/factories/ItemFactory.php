<?php

declare(strict_types=1);

/* @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Item;
use Faker\Generator as Faker;

$factory->define(Item::class, function (Faker $faker) {
    return [
        'sku' => $faker->ean8,
        'description' => $faker->realText($faker->numberBetween(15, 30)),
        'price' => $faker->randomFloat(2, 5, 100),
    ];
});
