<?php

declare(strict_types=1);

/* @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Customer;
use App\Item;
use App\Order;
use App\ShippingAddress;
use Faker\Generator as Faker;

$factory->define(Order::class, function (Faker $faker) {
    $customer = factory(Customer::class)->create();
    $shippingAddress = factory(ShippingAddress::class)->create([
        'customer_id' => $customer->id,
    ]);

    return [
        'customer_id' => $customer->id,
        'shipping_address_id' => $shippingAddress->id,
        'total' => 0,
    ];
});

$factory->afterCreating(Order::class, function (Order $order, Faker $faker) {
    factory(Item::class, $faker->numberBetween(1, 5))->create()->each(function (Item $item) use ($order, $faker) {
        $order->items()->attach($item->id, [
            'qty' => $faker->numberBetween(1, 5),
        ]);
    });
});
