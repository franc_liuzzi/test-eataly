<?php

declare(strict_types=1);

/* @var \Illuminate\Database\Eloquent\Factory $factory */
use App\ShippingAddress;
use Faker\Generator as Faker;

$factory->define(ShippingAddress::class, function (Faker $faker) {
    return [
        // 'customer_id',
        'street' => $faker->streetAddress,
        'city' => $faker->city,
        'zipcode' => $faker->postcode,
        'country' => $faker->country,
    ];
});
