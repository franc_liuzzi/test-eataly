<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    private $table_name = 'orders';
    private $procedure_name = 'refresh_order_total';

    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create($this->table_name, function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('customer_id')->unsigned()->index();
            $table->foreign('customer_id')->references('id')->on('customers');
            $table->bigInteger('shipping_address_id')->unsigned()->index();
            $table->foreign('shipping_address_id')->references('id')->on('shipping_addresses');
            $table->float('total');
            $table->timestamps();
        });

        // DB::unprepared("CREATE PROCEDURE `$this->procedure_name`(order_id INT)
        // BEGIN
        //     UPDATE $this->table_name SET total = (SELECT SUM(price * qty) FROM order_items INNER JOIN items ON items.id = order_items.item_id WHERE order_items.order_id = order_id) WHERE id = order_id;
        // END");
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        // DB::unprepared("DROP PROCEDURE IF EXISTS $this->procedure_name;");
        Schema::dropIfExists($this->table_name);
    }
}
