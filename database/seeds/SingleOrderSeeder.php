<?php

declare(strict_types=1);

use App\Customer;
use App\ShippingAddress;
use Illuminate\Database\Seeder;

class SingleOrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        factory(Customer::class, 2)->create()->each(function (Customer $customer) {
            $shippingAddresses = factory(ShippingAddress::class, 2)->make();
            $customer->shippingAddresses()->save($shippingAddresses);
        });
    }
}
